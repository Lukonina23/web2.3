<?php
// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  // В суперглобальном массиве $_GET PHP хранит все параметры, переданные в текущем запросе через URL.
  if (!empty($_GET['save'])) {
    // Если есть параметр save, то выводим сообщение пользователю.
    print('Спасибо, результаты сохранены.');
  }
  // Включаем содержимое файла form.php.
  include('form.php');
  // Завершаем работу скрипта.
  exit();
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.

// Проверяем ошибки.
$errors = FALSE;
if (empty($_POST['fio'])) {
  print('Заполните имя.<br/>');
  $errors = TRUE;
}
if (empty($_POST['email'])) {
  print('Заполните e-mail.<br/>');
  $errors = TRUE;
}
if (empty($_POST['biography'])) {
  print('Заполните биографию.<br/>');
  $errors = TRUE;
}



// *************
// Тут необходимо проверить правильность заполнения всех остальных полей.
// *************

if ($errors) {
  // При наличии ошибок завершаем работу скрипта.
  exit();
}

// Сохранение в базу данных.

$user = 'u20948';
$pass = '6000592';
$db = new PDO('mysql:host=localhost;dbname=u20948', $user, $pass, array(PDO::ATTR_PERSISTENT => true));



try {
  $str = implode(',',$_POST['sp-sp']);
  
  $stmt = $db->prepare("INSERT INTO appl SET fio = ?, email = ?, yob = ?, pol = ?, limb = ?, biograghy = ?");
  $stmt -> execute([$_POST['fio'],$_POST['email'],$_POST['yob'],$_POST['radio-pol'],$_POST['radio-kon'],$_POST['biography']]);

  $stmt = $db->prepare("INSERT INTO abilities SET abilities = ?");
  $stmt -> execute([$str]);
  
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}
header('Location: ?save=1');

